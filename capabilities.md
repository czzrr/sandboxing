# Capabilities

In older Linux kernels, privileges were all or nothing: either you were root with privileges to do anything, or you had no privileges at all.

Traditionally, set-user-ID (suid) executables were used to allow an unprivileged user to run a program that performs a privileged action, such as changing the system time.
The process could then switch back and forth (using saved suid) between the unprivileged and privileged user IDs (non-zero and 0) whenever it required or didn't require privileges.

Nowadays, privileges are divided into distinct units known as *capabilities*.

Each privileged operation has a corresponding capability.
For a process to perform a privileged operation, it must possess the corresponding capability of the operation, regardless of the process's effective user ID.

## Process and file capabilities.

Both processes and files have capabilities!

### Process capabilities

Each processes has five types of capability sets:

* **Permitted:** The permitted set of capabilities are the capabilities that a process *may* have.
The permitted set is a limiting superset of capabilities that *may be added* to the effective and inheritable sets by the process.
Once a capability is removed from the permitted set, it cannot be regained by the process on its own (but is possible by executing another program).
* **Effective:** The effective set of capabilities is inspected when the kernel checks the privileges for the process.
* **Inheritable:** The inheritable set of capabilities contains the capabilities that *may* be passed down onto programs executed by the process.
* **Bounding:**  The capability bounding set is used for restricting the capabilities a process can gain during `exec()`.
It is used in two ways:
  1. When calling `exec()`, capabilities in both the file's permitted set and the process's bounding set *are added* to the new process's permitted set.
  2. The bounding set is a limiting superset for the capabilities that *may be added* to the process's inheritable set.
* **Ambient:** The ambient capability set consists of capabilities that are preserved when `exec()`'ing an unprivileged program.
An ambient capability is both permitted and inheritable.

### File capabilities

The capability sets of a file determines which capabilities are given to the process executing the file.

Each file has three types of capability sets:

* **Permitted (forced):** The capabilities that *are added* to the permitted set of the process executing the file (with respect to the bounding set).
* **Effective:** This "set" is actually just a single bit. If enabled, the process' effective set gains the capabilities in the process's permitted set.
If disabled, the process's effective set is initially empty.
  * The effective bit should be enabled for programs that are capability-unaware, i.e., suid programs.
  * The effective bit should be disabled by capability-aware programs, allowing such programs to specify the required capabilities.
* **Inheritable (allowed):** Capabilities that are both in the file's inheritable set and process's inheritable set are added to the process's permitted set.



## Establishing capabilities during `exec()`

The rules for assigning capabilities to a process when calling `exec()` are:

```
P'(permitted) = (P(inheritable) & F(inheritable)) | (F(permitted) & cap_bset) | P'(ambient)
P'(effective) = F(effective) ? P'(permitted) : P'(ambient)
P'(inheritable) = P(inheritable)
P'(bounding) = P(bounding)
P'(ambient) = (file is privileged) ? 0 : P(ambient)
```

* `P()` is the value of a process capability set before `exec()`, 
* `P'()` is the value of a process capability set after `exec()`,
* `F()` is the value of a file capability set.

## Capabilities in action

### Capability-dumb

Suppose we wanted to write a program that can authenticate users from the standard password database.
We simulate this by opening and closing `/etc/shadow`.
First, we create the project:

```console
ca@ubu:~$ cargo new auth
ca@ubu:~$ cd auth
```

Then in `src/main.rs`, we write

```
fn main() {
    let _ = std::fs::File::open("/etc/shadow").unwrap();
    println!("Reading file...")
}
```

If we run this, we get

```console
ca@ubu:~/auth$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.00s
     Running `target/debug/auth`
thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 13, kind: PermissionDenied, message: "Permission denied" }', src/main.rs:2:48
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

This is because we don't have permission of opening `/etc/shadow`, since we are neither root nor in the shadow group:

```console
ca@ubu:~/auth$ ls -al /etc/shadow
-rw-r----- 1 root shadow 1581 May 12 22:44 /etc/shadow
```

Following the traditional approach, an option is to set the uid bit on the executable:

```console
ca@ubu:~/auth$ sudo chown root target/debug/auth
ca@ubu:~/auth$ sudo chmod u+s target/debug/auth
ca@ubu:~/auth$ ls -al target/debug/auth
-rwsrwxr-x 2 root ca 4325728 Jun  5 10:36 target/debug/auth
```

This allows us to run the executable:

```console
ca@ubu:~/auth$ cargo -q run
Reading file...
```

However, this gives the process the full set of capabilities.
We can check this by inserting a sleep command at the start of the program:

```console
fn main() {
    std::thread::sleep(std::time::Duration::from_secs(10));
    let _ = std::fs::File::open("/etc/shadow").unwrap();
    println!("Reading file...")
}
```

After rebuilding, changing file owner to root and setting the uid bit, we get

```console
ca@ubu:~/auth$ cargo -q run &
[5] 12635
ca@ubu:~/auth$ getpcaps 12635
12635: =ep
```

This is undesirable, as we only need permission to bypass permission checks when reading from `/etc/shadow`.

For this we can use the `CAP_DAC_READ_SEARCH` capability, which according to the `capabilities` man page "bypass file read permission checks and directory read and execute permission checks".

We run `cargo clean` and `cargo build` to reset the file status and then we set the capability on the file:

```console
ca@ubu:~/auth$ ls -al target/debug/auth
-rwxrwxr-x 2 ca ca 4327976 Jun  5 10:49 target/debug/auth
ca@ubu:~/auth$ sudo setcap 'cap_dac_read_search+ep' target/debug/auth
ca@ubu:~/auth$ getcap target/debug/auth
target/debug/auth cap_dac_read_search=ep
ca@ubu:~/auth$ cargo -q run &
[5] 12776
ca@ubu:~/auth$ getpcaps 12776
12776: cap_dac_read_search=ep
```

Now our process only gets the one capability it needs to successfully execute the program.
However, since the program is capability-dumb, it keeps the capability for the lifetime of the process.

### Capability-aware

An even more secure approach is to make the program capability-aware and make it drop the required capability for opening `/etc/shadow` right after it has opened it.

To work with capabilities in Rust, we can use the `capng` crate, which is a wrapper for the `libcap-ng` C library.
We add capability manipulation to our program:

```
fn main() {
    std::thread::sleep(std::time::Duration::from_secs(5));

    println!("Adding CAP_DAC_READ_SEARCH to the the effective set");
    // Clear capabilities
    capng::clear(capng::Set::CAPS);
    // Add CAP_DAC_READ_SEARCH to effective set
    capng::updatev(capng::Action::ADD, capng::Type::EFFECTIVE | capng::Type::PERMITTED, vec!["dac_read_search"]).unwrap();
    // Apply capability changes
    capng::apply(capng::Set::CAPS).unwrap();

    std::thread::sleep(std::time::Duration::from_secs(5));

    let _ = std::fs::File::open("/etc/shadow").unwrap();
    println!("Reading file...");
    println!("Removing CAP_DAC_READ_SEARCH from the effective set");
    capng::updatev(capng::Action::DROP, capng::Type::EFFECTIVE, vec!["dac_read_search"]).unwrap();
    capng::apply(capng::Set::CAPS).unwrap();

    std::thread::sleep(std::time::Duration::from_secs(5));
}
```

For the process to raise `CAP_DAC_READ_SEARCH` in its effective set, we need to add the capability to the file permitted set:

```console
ca@ubu:~/auth$ cargo b
   Compiling auth v0.1.0 (/home/ca/auth)
    Finished dev [unoptimized + debuginfo] target(s) in 0.16s
ca@ubu:~/auth$ sudo setcap 'cap_dac_read_search+p' target/debug/auth
ca@ubu:~/auth$ getcap target/debug/auth
target/debug/auth cap_dac_read_search=p
```

By executing the program and running the `getpcaps` command inbetween the calls to `std::thread::sleep()`, we can inspect the process's capabilities at various points:

```console
ca@ubu:~/auth$ cargo run -q &
[5] 16220
ca@ubu:~/auth$ getpcaps 16220
16220: cap_dac_read_search=p
ca@ubu:~/auth$ Adding CAP_DAC_READ_SEARCH to the the effective set
getpcaps 16220
16220: cap_dac_read_search=ep
ca@ubu:~/auth$ Reading file...
Removing CAP_DAC_READ_SEARCH from the effective set
getpcaps 16220
16220: cap_dac_read_search=p
ca@ubu:~/auth$ 
[5]   Done                    cargo run -q
```

By manipulating capabilities in this way, we can follow the *principle of least privilege* in a temporal manner by raising a capability when we need it and dropping it when we no longer need it.