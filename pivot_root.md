# pivot_root
`pivot_root` simultaneously changes the root filesystem and puts the old root filesystem somewhere accessible by the new root filesystem such that it is possible to unmount the old root filesystem.

First, we create a new mount namespace (namespaces are described later, but for now it is enough to know that this creates a copy of the list of mount points):

```console
ca@ubu:~$ sudo unshare -m
[sudo] password for ca: 
root@ubu:/home/ca# 
```

We then bind mount `new-root` inside that mount namespace, and run `pivot_root` to change the root filesystem to `new-root` and the old root filesystem to `new-root/old-root`:

```console
root@ubu:/home/ca# mount --bind new-root/ new-root/
root@ubu:/home/ca# cd new-root/
root@ubu:/home/ca/new-root# pivot_root . old-root/
root@ubu:/home/ca/new-root# cd /
root@ubu:/# ls
alpine-minirootfs-3.18.0-x86_64.tar.gz  home                                    old-root                                run                                     tmp
bin                                     lib                                     opt                                     sbin                                    usr
dev                                     media                                   proc                                    srv                                     var
etc                                     mnt                                     root                                    sys
```

We need to mount `proc` to see the list of mount points:

```console
root@ubu:/# /bin/mount
mount: no /proc/mounts
root@ubu:/# /bin/mount -t proc proc /proc
root@ubu:/# /bin/mount
/dev/sda3 on /old-root type ext4 (rw,relatime,errors=remount-ro)
udev on /old-root/dev type devtmpfs (rw,nosuid,relatime,size=4027440k,nr_inodes=1006860,mode=755,inode64)
...
/dev/sda3 on / type ext4 (rw,relatime,errors=remount-ro)
proc on /proc type proc (rw,relatime)
```

We see the old root filesystem mounted at `/old-root`.
This is because by default, the mount points in the new mount namespace is a clone of the mount points of the parent process' mount namespace.
To isolate our process in the new mount namespace, we would like to unmount the old root:

```console
root@ubu:/# umount old-root/
umount: can't unmount old-root/: Resource busy
```

However, we can't do that, as a bunch of processes are still running under the old root:

```console
root@ubu:/# fuser -m old-root/
1 2 3 4 5 6 7 8 10 11 12 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29 31 32 33 34 35 37 38 39 40 41 43 44 45 46 47 49 50 51 52 53 55 56 57 58 59 61 63 64 65 66 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 91 94 96 97 98 99 100 101 102 104 105 106 107 112 113 114 120 121 167 169 170 194 207 213 220 221 225 226 227 230 231 260 261 304 335 356 357 358 359 360 361 362 363 364 365 404 410 424 600 603 727 728 731 732 733 735 742 743 746 749 751 756 758 761 762 763 766 772 835 837 852 864 942 943 944 945 946 1054 1066 1079 1097 1131 1135 1136 1137 1185 1186 1189 1190 1194 1198 1200 1204 1222 1223 1253 1306 1325 1345 1350 1358 1414 1417 1423 1498 1530 1542 1575 1583 1584 1595 1596 1603 1606 1611 1617 1622 1631 1637 1643 1649 1650 1658 1666 1680 1689 1691 1707 1708 1710 1711 1713 1715 1717 1718 1720 1724 1736 1737 1738 1739 1740 1746 1779 1796 1819 1820 1857 1863 1867 1870 1871 1882 1884 1889 1890 1894 1912 1915 1922 1943 1954 1959 1987 1989 2009 2043 2126 2144 2201 2322 2348 2818 3484 3486 3520 3589 3714 3718 3719 3721 3735 3751 3771 3783 3821 3837 3846 3847 3858 3910 3960 4081 4082 4083 4112 
```

We need to unmount it lazily with the `-l` flag:

```console
root@ubu:/# umount -l old-root/
root@ubu:/# /bin/mount
/dev/sda3 on / type ext4 (rw,relatime,errors=remount-ro)
proc on /proc type proc (rw,relatime)
```

Our process does not have access to the old root filesystem anymore!

However, it can still list them:

```console
root@ubu:/# cat proc/1/mounts
sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
udev /dev devtmpfs rw,nosuid,relatime,size=4027440k,nr_inodes=1006860,mode=755,inode64 0 0
devpts /dev/pts devpts rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
tmpfs /run tmpfs rw,nosuid,nodev,noexec,relatime,size=813880k,mode=755,inode64 0 0
/dev/sda3 / ext4 rw,relatime,errors=remount-ro 0 0
..
proc /home/ca/new-root/proc proc rw,relatime 0 0
```

What we need is for our process to have its own `proc` filesystem...

## NOTE

This does not work:

```console
ca@ubu:~$ sudo unshare -m
root@ubu:/home/ca# cd new-root/
root@ubu:/home/ca/new-root# mount . . --bind
root@ubu:/home/ca/new-root# pivot_root . old-root/
pivot_root: failed to change root from `.' to `old-root/': Device or resource busy
```

But if you `cd . ` before changing root, it works:

```console
ca@ubu:~$ sudo unshare -m
root@ubu:/home/ca# cd new-root/
root@ubu:/home/ca/new-root# mount . . --bind
root@ubu:/home/ca/new-root# cd .
root@ubu:/home/ca/new-root# pivot_root . old-root/
root@ubu:/home/ca/new-root# cd /
root@ubu:/# ls
alpine-minirootfs-3.18.0-x86_64.tar.gz  etc                                     mnt                                     root                                    sys
bin                                     home                                    old-root                                run                                     tmp
chroot                                  lib                                     opt                                     sbin                                    usr
dev                                     media                                   proc                                    srv                                     var
root@ubu:/# 
```

This also works:

```console
ca@ubu:~$ sudo unshare -m
root@ubu:/home/ca# cd new-root/
root@ubu:/home/ca/new-root# mount . . --bind
root@ubu:/home/ca/new-root# pivot_root /home/ca/new-root /home/ca/new-root/old-root/
root@ubu:/home/ca/new-root# cd /
root@ubu:/# ls
alpine-minirootfs-3.18.0-x86_64.tar.gz  etc                                     mnt                                     root                                    sys
bin                                     home                                    old-root                                run                                     tmp
chroot                                  lib                                     opt                                     sbin                                    usr
dev                                     media                                   proc                                    srv                                     var
root@ubu:/# 
```
