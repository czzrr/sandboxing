# User namespaces and capabilities

**Note:** The programs discussed in this page are modifications of programs from [https://lwn.net/Articles/531114/](https://lwn.net/Articles/531114/).

## Capabilities of first process in a user namespace

Calling `clone(CLONE_NEWUSER)` spawns a process in a new user namespace.
The newly spawned process has a full set of capabilities in the newly created user namespace.
This is shown by the program `userns_clone.c`, which creates a new child process in a new user namespace using `clone(CLONE_NEWUSER)`:

```console
ca@ubu:~/work/sandboxing$ gcc userns_clone.c -o userns_clone -lcap
ca@ubu:~/work/sandboxing$ ./userns_clone 
capabilities: =ep
```

Calling `unshare(CLONE_NEWUSER)` will create a new user namespace and place the calling process in it.
The process has a full set of capabilities in the new child user namespace, but none in the old parent user namespace.
This is shown by the program `userns_unshare.c`, which creates a new user namespace using `unshare(CLONE_NEWUSER)`:

```console
ca@ubu:~/work/sandboxing$ ./userns_unshare 
capabilities: =ep
```

Similar to `unshare()`, a process joining a user namespace using `setns()` will have a full set of capabilities in the joined namespace.

Even though a process has a full set of capabilities in the new user namespace, it has no capabilities whatsoever in the parent or previous user namespace.

## When does a process have capabilities in a user namespace?

1. A process has a capability in a user namespace if the process is in the user namespace and has the capability in its effective set.

2. A process with a capability in a user namespace has the same capability in all descendant user namespaces.

3. The effective user ID of a process creating a user namespace is saved by the kernel as the owner of the namespace.
A process that has the same effectiver user ID as the owner and is within the parent user namespace has a full set of capabilities in the child user namespace.

## User namespaces and non-user namespaces

* When a non-user namespace is created, it is assigned an owner user namespace.
Its owner is the user namespace in which the creating process was in at the point of creating the non-user namespace.

* A process can perform privileged operations on resources managed by non-user namespaces if the process has the required capabilities in the user namespace that owns the non-user namespace.

The program `userns_unshare_hostname.c` demonstrates this.
It uses `unshare()` to create a user namespace and, if given the flag `-u`, additionally creates a UTS namespace.
The program displays the capabilities and hostname, attempts to set the hostname to `bella` and displays the new hostname.
Whether or not updating the hostname succeeds depends on the capabilities in the user namespace that owns the UTS namespace:

```console
ca@ubu:~/work/sandboxing$ ./userns_unshare_hostname 
Creating user namespace
capabilities: =ep
hostname: ubu
sethostname: Operation not permitted
ca@ubu:~/work/sandboxing$ ./userns_unshare_hostname -u
Creating user and UTS namespace
capabilities: =ep
hostname: ubu
hostname: bella
```

When the `-u` flag is omitted, setting the hostname fails because after calling `unshare(CLONE_NEWUSER)`, the process has no capabilities in the initial user namespace, which is the owner of the namespace that manages the resources we are trying to operate on, namely the initial UTS namespace.

With the `-u` flag, the new user namespace owns the new UTS namespace, and since we have a full set of capabilities in the new user namespace, we are allowed to set the hostname managed by the new UTS namespace.