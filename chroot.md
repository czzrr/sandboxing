# chroot

`chroot` is a program that spawns a process with a specified root directory.

Let's try it!

```console
ca@ubu:~$ mkdir new-root
ca@ubu:~$ sudo chroot new-root sh
chroot: failed to run command ‘sh’: No such file or directory
```

The error is justified, as `new-root` is empty.

An easy way to get started is to download an Alpine Linux filesystem:

```console
ca@ubu:~$ cd new-root
ca@ubu:~/new-root$ wget https://dl-cdn.alpinelinux.org/alpine/v3.18/releases/x86_64/alpine-minirootfs-3.18.0-x86_64.tar.gz
ca@ubu:~/new-root$ tar -xf alpine-minirootfs-3.18.0-x86_64.tar.gz 
ca@ubu:~/new-root$ ls
alpine-minirootfs-3.18.0-x86_64.tar.gz  dev  home  media  opt   root  sbin  sys  usr
bin                                     etc  lib   mnt    proc  run   srv   tmp  var
```

Let's try again:

```console
ca@ubu:~/new-root$ cd ..
ca@ubu:~$ sudo chroot new-root sh
/ # ls
alpine-minirootfs-3.18.0-x86_64.tar.gz  proc
bin                                     root
dev                                     run
etc                                     sbin
home                                    srv
lib                                     sys
media                                   tmp
mnt                                     usr
opt                                     var
```

Surely enough, we see that the root directory of the spawned shell is `~/chroot-test/new-root`, since that's where we put the `.tar.gz` file.

Can we run `ps`?

```console
/ # ps
PID   USER     TIME  COMMAND
/ #
```

Nothing shows... is `proc` even mounted?

```console
/ # mount
mount: no /proc/mounts
```

Nope! Let's mount it then.

```console
/ # mount -t proc proc /proc
/ # mount
proc on /proc type proc (rw,relatime)
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:03 {systemd} /sbin/init splash
    2 root      0:00 [kthreadd]
    3 root      0:00 [rcu_gp]
...
66056 1000      0:00 {Web Content} /snap/firefox/2356/usr/lib/firefox/firefox -contentproc -childID 3
66896 root      0:00 [kworker/6:0-eve]
66897 root      0:00 [kworker/1:0-eve]
67043 root      0:00 [kworker/u16:2-e]
67142 root      0:00 [kworker/u16:1-f]
67312 root      0:00 [kworker/u16:3-e]
67452 root      0:00 sudo chroot new-root sh
67453 root      0:00 sudo chroot new-root sh
67454 root      0:00 sh
67482 root      0:00 ps
```

The shell is certainly not running Firefox - we see everything happening on the system, even when we are inside our own root directory.

Furthermore, after mounting `proc`, we can list the mount points of the mount namespace in which, e.g, the `init` process resides (which is the initial mount namespace):

```console
/ # cat /proc/1/mounts
sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
...
proc /home/ca/new-root/proc proc rw,relatime 0 0
```

We definitely don't want a sandboxed process to be able to have a global view of mounts and processes.