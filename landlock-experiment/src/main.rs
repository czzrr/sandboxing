
mod uapi;

use uapi::*;
use libc::{open, O_PATH, O_CLOEXEC};
use std::ffi::CString;

fn main() {
    let ruleset_attr = landlock_ruleset_attr {
        handled_access_fs: (LANDLOCK_ACCESS_FS_EXECUTE
            | LANDLOCK_ACCESS_FS_WRITE_FILE
            | LANDLOCK_ACCESS_FS_READ_FILE
            | LANDLOCK_ACCESS_FS_READ_DIR
            | LANDLOCK_ACCESS_FS_REMOVE_DIR
            | LANDLOCK_ACCESS_FS_REMOVE_FILE
            | LANDLOCK_ACCESS_FS_MAKE_CHAR
            | LANDLOCK_ACCESS_FS_MAKE_DIR
            | LANDLOCK_ACCESS_FS_MAKE_REG
            | LANDLOCK_ACCESS_FS_MAKE_SOCK
            | LANDLOCK_ACCESS_FS_MAKE_FIFO
            | LANDLOCK_ACCESS_FS_MAKE_BLOCK
            | LANDLOCK_ACCESS_FS_MAKE_SYM) as u64,
    };

    let abi =
        unsafe { landlock_create_ruleset(std::ptr::null(), 0, LANDLOCK_CREATE_RULESET_VERSION) };
    if abi < 0 {
        panic!("The running kernel does not enable to use Landlock");
    }
    
    let ruleset_fd = unsafe {
        landlock_create_ruleset(
            &ruleset_attr as *const _,
            std::mem::size_of::<landlock_ruleset_attr>(),
            0,
        )
    };
    if ruleset_fd < 0 {
        panic!("Failed to create a ruleset");
    }

    let mut path_beneath = landlock_path_beneath_attr {
        allowed_access: (LANDLOCK_ACCESS_FS_EXECUTE
            | LANDLOCK_ACCESS_FS_READ_FILE
            | LANDLOCK_ACCESS_FS_READ_DIR) as u64,
        parent_fd: 0,
    };
    let target = CString::new("/usr").unwrap();
    path_beneath.parent_fd = unsafe { open(target.as_ptr(), O_PATH | O_CLOEXEC) };
    if path_beneath.parent_fd < 0 {
        unsafe { libc::close(ruleset_fd) };
        panic!("Failed to open file");
    }

    let err = unsafe {
        landlock_add_rule(
            ruleset_fd,
            landlock_rule_type_LANDLOCK_RULE_PATH_BENEATH,
            &path_beneath as *const landlock_path_beneath_attr as *const _,
            0,
        )
    };
    if err != 0 {
        unsafe { libc::close(ruleset_fd) };
        panic!("Failed to update ruleset");
    }

    let ret = unsafe { libc::prctl(libc::PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0) };
    if ret != 0 {
        unsafe { libc::close(ruleset_fd) };
        panic!("Failed to restrict privileges");
    }
    
    let ret = unsafe { landlock_restrict_self(ruleset_fd, 0) };
    if ret != 0 {
        unsafe { libc::close(ruleset_fd) };
        panic!("Failed to enforce ruleset");
    }
    unsafe { libc::close(ruleset_fd) };

    println!("LandLock'ed!");

    assert!(std::fs::read_dir("/usr").is_ok());
    assert!(std::fs::read_dir("/tmp").is_err());
}
