# Landlock experiment

This small program exercises the LandLock Linux kernel feature through the example program described at [https://docs.kernel.org/userspace-api/landlock.html](https://docs.kernel.org/userspace-api/landlock.html).

`src/uapi/landlock.rs` is generated from `/usr/include/linux/landlock.h` on my system with `rust_bindgen`.
This should ideally be the latest version.

## What is LandLock?
LandLock ([https://landlock.io/](https://landlock.io/)) is a Linux kernel security module that provides the functionality for unprivileged processes to isolate themselves. 

## LandLock rules
With LandLock, an unprivileged process can restrict its filesystem access rights with *rules*.
A rule is an access right of a filesystem hierarchy.
A ruleset is a set of rules that can be enforced to restrict the process and all its future children.

## Defining and enforcing a security policy
First, we define the access rights we want our ruleset to be able to handle.
To define a ruleset that can handle all access rights:

```rust
let ruleset_attr = landlock_ruleset_attr {
    handled_access_fs: (LANDLOCK_ACCESS_FS_EXECUTE
        | LANDLOCK_ACCESS_FS_WRITE_FILE
        | LANDLOCK_ACCESS_FS_READ_FILE
        | LANDLOCK_ACCESS_FS_READ_DIR
        | LANDLOCK_ACCESS_FS_REMOVE_DIR
        | LANDLOCK_ACCESS_FS_REMOVE_FILE
        | LANDLOCK_ACCESS_FS_MAKE_CHAR
        | LANDLOCK_ACCESS_FS_MAKE_DIR
        | LANDLOCK_ACCESS_FS_MAKE_REG
        | LANDLOCK_ACCESS_FS_MAKE_SOCK
        | LANDLOCK_ACCESS_FS_MAKE_FIFO
        | LANDLOCK_ACCESS_FS_MAKE_BLOCK
        | LANDLOCK_ACCESS_FS_MAKE_SYM) as u64,
};
```

We do this if we want to, e.g., allow read actions and deny write actions.
Write actions are included for compatibility reasons (i.e. the kernel and user space may not know each other's supported restrictions) (?).

To actually create the ruleset:

```rust
let ruleset_fd = unsafe {
    landlock_create_ruleset(
        &ruleset_attr as *const _,
        std::mem::size_of::<landlock_ruleset_attr>(),
        0,
    )
};
if ruleset_fd < 0 {
    panic!("Failed to create a ruleset");
}
```

The newly created ruleset is initially empty, so we will add a rule.
Currently, LandLock only supports one kind of rule, namely `LANDLOCK_RULE_PATH_BENEATH`, which controls access within and below a given directory.
To define the actions we wish to apply to some directory hierarchy:

```rust
let mut path_beneath = landlock_path_beneath_attr {
    allowed_access: (LANDLOCK_ACCESS_FS_EXECUTE
        | LANDLOCK_ACCESS_FS_READ_FILE
        | LANDLOCK_ACCESS_FS_READ_DIR) as u64,
    parent_fd: 0,
};
```

`parent_fd` must be set to the file descriptor of the directory we wish to apply the rules to, e.g., `/usr`:

```rust
let target = CString::new("/usr").unwrap();
    path_beneath.parent_fd = unsafe { open(target.as_ptr(), O_PATH | O_CLOEXEC) };
if path_beneath.parent_fd < 0 {
    unsafe { libc::close(ruleset_fd) };
    panic!("Failed to open file");
}
```

To add the rule to the ruleset:

```rust
let err = unsafe {
        landlock_add_rule(
            ruleset_fd,
            landlock_rule_type_LANDLOCK_RULE_PATH_BENEATH,
            &path_beneath as *const landlock_path_beneath_attr as *const _,
            0,
        )
    };
if err != 0 {
    unsafe { libc::close(ruleset_fd) };
    panic!("Failed to update ruleset");
}
```

The rule is now added to the ruleset.
To actually enforce the rule, we first need to disallow the process to obtain any new capabilities:

```rust
let ret = unsafe { libc::prctl(libc::PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0) };
if ret != 0 {
    unsafe { libc::close(ruleset_fd) };
    panic!("Failed to restrict privileges");
}
```

We can now enforce the rule:

```rust
let ret = unsafe { landlock_restrict_self(ruleset_fd, 0) };
if ret != 0 {
    unsafe { libc::close(ruleset_fd) };
    panic!("Failed to enforce ruleset");
}
unsafe { libc::close(ruleset_fd) };

println!("LandLock'ed!");
```

The process is now restricted to reading and executing files within the `/usr` directory hierarchy, and all its future children will have this restriction as well.
For example, this means that we can list the immediate files of `/usr` but not those of `/tmp`:

```rust
assert!(std::fs::read_dir("/usr").is_ok());
assert!(std::fs::read_dir("/tmp").is_err());
```

There is no way to remove an enforced security policy for the remainder of the process' lifetime; it is only allowed to add more restrictions.