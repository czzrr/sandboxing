# Namespaces

- [What are namespaces?](#what-are-namespaces)
- [UTS namespaces](#uts-namespaces)
- [Mount namespaces](#mount-namespaces)
  - [Mount propagation types](#mount-propagation-types)
  - [Peer groups](#peer-groups)
- [PID namespaces](#pid-namespaces)
  - [PID namespaces hierarchy](#pid-namespaces-hierarchy)
- [Network namespaces](#network-namespaces)
- [User namespaces](#user-namespaces)
  - [Viewing user ID mappings](#viewing-user-id-mappings)

## What are namespaces?

Namespaces are a Linux kernel feature that offers groups of processes to have their own isolated view of certain types of resources.

Currently 7 namespace types are supported:

- UTS namespace
  - Isolates the hostname
- Mount namespace
  - Isolates the set of mount points
- PID namespace
  - Isolates the PID allocation to processes in the namespace
- User namespace
  - Isolates user IDs and group IDs
- Network namespace
  - Isolates network devices, port numbers etc.
- Cgroups namespace
  - Isolates cgroups, which are used for restricting resources such as memory and CPU
- IPC namespace
  - Isolates the set of inter-process communication (IPC) objects
- Time namespace
  - Isolates the system clock

`unshare` is the command that creates various types of namespaces.

## UTS namespaces

```console
# shell 1
ca@ubu:~$ hostname
ubu
```

We can create an UTS namespace by calling `unshare` with the `-u` flag:
```console
# shell 2
ca@ubu:~$ sudo unshare -u
[sudo] password for ca: 
root@ubu:/home/ca# hostname
ubu
root@ubu:/home/ca# hostname bella
root@ubu:/home/ca# hostname
bella
```

The hostname in shell 2 is `bella`, while it is still `ubu` in shell 1.

We can view the  UTS namespace identifier for each shell:

```console
# shell 1
ca@ubu:~$ readlink /proc/$$/ns/uts
uts:[4026531838]
```

```console
# shell 2
root@ubu:/home/ca# readlink /proc/$$/ns/uts
uts:[4026532437]
```

Processes in the same UTS namespace will see the same hostname:

```console
# shell 1
ca@ubu:~$ sleep 60 &
[1] 5787
ca@ubu:~$ readlink /proc/5787/ns/uts
uts:[4026531838]
```

```console
# shell 2
root@ubu:/home/ca# sleep 60 &
[1] 5807
root@ubu:/home/ca# readlink /proc/5807/ns/uts
uts:[4026532437]
```

## Mount namespaces

We create a mount namespace by calling `unshare` with the `-m` flag.
What we get is a copy of the mount point list of the previous mount namespace:

```console
# shell 2
ca@ubu:~$ sudo unshare -m
root@ubu:/home/ca# mount -t proc proc /proc
root@ubu:/home/ca# mount
/dev/sda3 on / type ext4 (rw,relatime,errors=remount-ro)
...
/dev/sr0 on /media/ca/VBox_GAs_6.1.38 type iso9660 (ro,nosuid,nodev,relatime,nojoliet,check=s,map=n,blocksize=2048,uid=1000,gid=1000,dmode=500,fmode=400,iocharset=utf8,uhelper=udisks2)
proc on /proc type proc (rw,relatime)
```

Then we mount a tmpfs fileystem to `/mytmpfs` inside the new mount namespace:

```console
# shell 2
root@ubu:/home/ca# mkdir /mytmpfs
root@ubu:/home/ca# mount -t tmpfs -o size=10M tmpfs /mytmpfs
root@ubu:/home/ca# cat /proc/$$/mountinfo
...
981 958 0:22 / /proc rw,nosuid,nodev,noexec,relatime - proc proc rw
...
1001 958 11:0 / /media/ca/VBox_GAs_6.1.38 ro,nosuid,nodev,relatime - iso9660 /dev/sr0 ro,nojoliet,check=s,map=n,blocksize=2048,uid=1000,gid=1000,dmode=500,fmode=400,iocharset=utf8
1002 981 0:46 / /proc rw,relatime - proc proc rw
1003 958 0:47 / /mytmpfs rw,relatime - tmpfs tmpfs rw,size=10240k,inode64
```

According to the [`mount_namespaces` man page](https://man7.org/linux/man-pages/man7/mount_namespaces.7.html), `/proc/[pid]/mounts,` `/proc/[pid]/mountinfo`, and `/proc/[pid]/mountstats` show the same information for all processes in the same mount namespace.
Here we use `/proc/$$/mountinfo` to see the mount point ids of the respective mount namespaces the shells reside in.
```console
# shell 1
ca@ubu:~$ cat /proc/$$/mountinfo
...
24 28 0:22 / /proc rw,nosuid,nodev,noexec,relatime shared:12 - proc proc rw
...
2340 28 11:0 / /media/ca/VBox_GAs_6.1.38 ro,nosuid,nodev,relatime shared:747 - iso9660 /dev/sr0 ro,nojoliet,check=s,map=n,blocksize=2048,uid=1000,gid=1000,dmode=500,fmode=400,iocharset=utf8
```

The mount points in shell 2 have different ids (the first field) from the mount points in shell 1.
Furthermore, shell 1 can't see the `/mytmpfs` and `/proc` mounts of shell 2.

### Mount propagation types
Mount points can have different *propagation types*, which control how changes in a mount point affect other mount points.
Propagation types makes it possible for two mount points in two different namespaces to send/receive mount and unmount events to/from each other.

The propagation type is a per-mount-point setting, and there are four of them:

* **Shared:** 
A mount point of this propagation type will share events with other mount points in its *peer group*. Sharing goes both ways: the other mounts points in the peer group will also share events to the mount point.
* **Private:**
A private mount point will neither send nor share events to any other mount point.
* **Slave:**
A mount point can be a *slave* of a *master* shared peer group from which it will receive events, but it will never itself send events to any mount point in the master peer group.
* **Unbindable:**
A mount point that is *unbindable* is both a private mount point and cannot be bind-mounted.

The propagation type only determines events directly beneath the mount point.
For example, if X is a shared mount and Y is mounted directly under X, other mount points in the peer group of X will see that Y has been mounted.
The propagation type of X has nothing to say about what happens on mount/unmount events under Y; that would depend on the propagation type of Y.
Similarly, propagation of an unmount event for when X is unmounted depends on the propagation type of the parent mount of X.

### Peer groups

As hinted above, a peer group is a set of mount points that propagate mount/unmount events to each other.
A peer group obtains new members when 1) a shared mount point is cloned when creating a new mount namespace, or 2) bind-mounting a shared mount point.
In both cases, the new mount point joins the same peer group as the original mount point.

Let's look at an example of peer groups.
First, we make the root mount point private and create two new shared mount points.

**TODO**

## PID namespaces

PID namespaces are created by passing `-p` to `unshare`.

```console
# shell 2
ca@ubu:~$ sudo unshare -p sh
# whoami
root
# ls
sh: 2: Cannot fork
# ps
sh: 3: Cannot fork
```

Huh? Looks like `whoami` ran with PID 1...
There is a hint in the man page for `unshare`:
```
-f, --fork
           Fork the specified program as a child process of unshare rather than running it directly. This is useful when creating a
           new PID namespace. [...]
```

Let's have a picture of what's going on first.

```console
# shell 2
ca@ubu:~$ sudo unshare -p sh
# sleep 60
```

```console
# shell 1
ca@ubu:~$ ps fa
    PID TTY      STAT   TIME COMMAND
   2756 pts/1    Ss     0:00 bash
   2781 pts/1    S+     0:00  \_ sudo unshare -p sh
   2782 pts/3    Ss     0:00      \_ sudo unshare -p sh
   2783 pts/3    S      0:00          \_ sh
```

We see that `sh` is not a child process of `unshare`, but of `sudo`.
This means that when executing `whoami` in shell 2, it will have process ID 1,Process ID 1 is special in the sense that it is the ID for the "init" process of a PID namespace and is expected to always be running.
Hence shell 2 errors out when `whoami` has finished executing, and no more processes can be spawned.

We can fix with by adding the `-f` flag when creating our PID namespace:
```console
# shell 2
ca@ubu:~$ sudo unshare -pf sh
# whoami
root
# ls
chroot-test  Desktop	Downloads  new-root  Public	      shareddir  Templates  vmi.sh
core.1894    Documents	Music	   Pictures  qemuvirtiofs.sh  snap	 Videos     work
# ps
    PID TTY          TIME CMD
   2892 pts/2    00:00:00 sudo
   2893 pts/2    00:00:00 unshare
   2894 pts/2    00:00:00 sh
   2901 pts/2    00:00:00 ps
```

In shell 1 we do in fact see that `sh` is now a child process of `unshare`:

```console
# shell 1
ca@ubu:~$ ps fa
    PID TTY      STAT   TIME COMMAND
   2756 pts/1    Ss     0:00 bash
   2891 pts/1    S+     0:00  \_ sudo unshare -pf sh
   2892 pts/2    Ss     0:00      \_ sudo unshare -pf sh
   2893 pts/2    S      0:00          \_ unshare -pf sh
   2894 pts/2    S+     0:00              \_ sh
```

Ok, great, we can spawn processes in our new PID namespace.
However, we would surely expect that running `ps` show PIDs starting at 1, no?
The reason for this is that the `proc` filesystem of the initial PID namespace is still mounted at `/proc`, which is where `ps` looks for process information:

```console
# shell 2
ca@ubu:~$ sudo unshare -pf
root@ubu:/home/ca# ps
    PID TTY          TIME CMD
   2976 pts/2    00:00:00 sudo
   2977 pts/2    00:00:00 unshare
   2978 pts/2    00:00:00 bash
   2991 pts/2    00:00:00 ps
root@ubu:/home/ca# ls /proc
1     1111  1318  1584  1690  1829  21    23    2869  348  49   65   74   84          dynamic_debug  meminfo        timer_list
10    1158  1322  1593  1692  1833  212   2333  2870  349  5    67   741  85          execdomains    misc           tty
[...]
```

We can mount our own `proc` filesystem to another location and inspect the processes running in our new PID namespace:

```console
# shell 2
root@ubu:/home/ca# mkdir /pidnsproc
root@ubu:/home/ca# mount -t proc proc /pidnsproc
root@ubu:/home/ca# ls /pidnsproc/
1           cgroups    dma            interrupts  key-users    mdstat   pagetypeinfo  softirqs       timer_list         zoneinfo
20          cmdline    driver         iomem       kmsg         meminfo  partitions    stat           tty
acpi        consoles   dynamic_debug  ioports     kpagecgroup  misc     pressure      swaps          uptime
[...]
```

We see process IDs 1 (`bash`) and 20 (`ls`), matching our expectations that process ID allocation is confined to our new PID namespace.

**NOTE:** If we mount `proc` on `/proc` in the child PID namespace, we mess up the process overview of the parent PID namespace, since the process IDs of its processes will be hidden by the `/proc` mount point of the child PID namespace.

For `ps` to work with only the processes in the child PID namespace, we have two options.

The first option is to use a new mount namespace in addition to the PID namespace.
This allows us to mount the child PID namespace `proc` file system at `/proc` without affecting the `proc` mount at `/proc` in the parent PID namespace:

```console
# shell 2
ca@ubu:~$ sudo unshare -mpf
root@ubu:/home/ca# mount -t proc proc /proc
root@ubu:/home/ca# ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 17:27 pts/2    00:00:00 -bash
root          15       1  0 17:27 pts/2    00:00:00 ps -ef
```

**NOTE:** Apparently, there is no need to first unmount the `/proc` mount point of the parent PID namespace, as mounting `/proc` within the new PID namespace will "override" it.

The second option is to use `chroot` to have a new root directory in which we can point `proc` at `/proc`:

```console
# shell 2
ca@ubu:~$ sudo unshare -mpf
root@ubu:/home/ca# chroot new-root sh
/ # mount
mount: no /proc/mounts
/ # mount -t proc proc /proc
/ # mount
proc on /proc type proc (rw,relatime)
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 -bash
   13 root      0:00 sh
   17 root      0:00 ps
```

### PID namespaces hierarchy

PID namespaces form a hierarchy in which the only PIDs a PID namespace can see is the PIDs of 1) the processes in its own PID namespace, and 2) the PIDs of processes in PID namespaces that are descendants (child, child of child, etc.).
A PID namespace cannot see PIDs in any of its ancestors (parent, parent of parent, etc.).

A process can have different PIDs depending on which namespace it is observed in.

Below, the `bash` process has PID 1 in the child PID namespace...

```console
# shell 2
ca@ubu:~$ sudo unshare -mpf
root@ubu:/home/ca# mount -t proc proc /proc
root@ubu:/home/ca# ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 20:38 pts/2    00:00:00 -bash
root          14       1  0 20:38 pts/2    00:00:00 ps -ef
```

... And has PID 4356 in the parent PID namespace:

```console
# shell 1
ca@ubu:~$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
...
root        4355    4354  0 20:38 pts/2    00:00:00 unshare -mpf
root        4356    4355  0 20:38 pts/2    00:00:00 -bash
ca          4370    2333  0 20:38 pts/0    00:00:00 ps -ef
```

Furthermore, notice that the parent PID (PPID) of the `bash` process in the child PID namespace is 0.
This is because the `unshare -mpf` parent process (with PID 4355) is in the parent PID namespace, which is isolated from the child PID namespace:

```console
# shell 1
ca@ubu:~$ sudo readlink /proc/4355/ns/pid
pid:[4026531836]
ca@ubu:~$ sudo readlink /proc/4356/ns/pid
pid:[4026532438]
```

## Network namespaces

Let's have a brief look at network namespaces.

A network namespace can be created by

```console
ca@ubu:~$ sudo ip netns add netns1
```

This creates a bind mount at `/var/run/netns`:

```console
ca@ubu:~$ ls /var/run/netns/ -l
total 0
-r--r--r-- 1 root root 0 Jun  2 17:59 netns1
```

By running the `ip` command with `-n` as flag, we can execute commands within the network namespace:

```console
ca@ubu:~$ sudo ip netns exec netns1 ip link list
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
```

Virtual network devices can be assigned to a network namespace, allowing processes within the namespace to communicate over the network.

A first step is to bring up the loopback device in `netns1`, as it is down upon creating the namespace:

```console
ca@ubu:~$ sudo ip netns exec netns1 ping 127.0.0.1
ping: connect: Network is unreachable
a@ubu:~$ sudo ip netns exec netns1 ip link set dev lo up
ca@ubu:~$ sudo ip netns exec netns1 ip link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
ca@ubu:~$ sudo ip netns exec netns1 ping 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.026 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.072 ms
^C
```

To allow for communication between the initial/root network namespace and `netns1`, we can create a virtual ethernet (veth) device.
First we create a pair of connected virtual ethernet devices:

```console
ca@ubu:~$ sudo ip link add veth0 type veth peer name veth1
```

Then we assign the veth device `veth1` to the network namespace `netns1`:

```console
ca@ubu:~$ sudo ip link set veth1 netns netns1
```

We now bring `veth1` up in `netns1` and `veth0` up in the initial network namespace:

```console
ca@ubu:~$ sudo ip netns exec netns1 ifconfig veth1 10.1.1.1/24 up
ca@ubu:~$ sudo ifconfig veth0 10.1.1.2/24 up
```

The two network namespaces can now ping each other!

```console
ca@ubu:~$ ping 10.1.1.1
PING 10.1.1.1 (10.1.1.1) 56(84) bytes of data.
64 bytes from 10.1.1.1: icmp_seq=1 ttl=64 time=0.057 ms
...
ca@ubu:~$ sudo ip netns exec netns1 ping 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=1 ttl=64 time=0.041 ms
...
```

## User namespaces

User namespaces isolate user IDs and group IDs.
A process can have different IDs inside and outside the namespace.
As a special case, this means that an unprivileged process (non-zero user ID) in the outer namespace can be privileged (user ID 0) within the namespace.
User namespaces are hierarchical like PID namespaces.

User namespaces is the only type of namespaces that does not require privilege to create:

```console
ca@ubu:~$ id -u
1000
ca@ubu:~$ unshare -U
nobody@ubu:~$ id -u
65534
nobody@ubu:~$ id -g
65534
```

We didn't specify a mapping from IDs in the child namespace to IDs in the parent namespace, so the default user and group IDs are 65534 (this number originates from `/proc/sys/kernel/overflowuid` and `/proc/sys/kernel/overflowgid`).

We can define ID mappings by writing to `/proc/[PID]/uid_map` and `/proc/[PID]/gid_map`.
These files contain one or more lines of the format

```
ID-inside-ns   ID-outside-ns   length
```

`ID-inside-ns` and `length` define a range of IDs in the child namespace that is mapped to a range of IDs of the same length in the parent namespace with starting point `ID-outside-ns`.

`ID-outside-ns` depends on in which user namespace the process opening `/proc/[PID]/uid_map` or `/proc/[PID]/gid_map` resides:

* If the opening process is in the same user namespace as process `PID`, then `ID-outside-ns` is an ID in the parent user namespace.
* If the opening process is in a different user namespace as process `PID`, then 
`ID-outside-ns` is an ID in the user namespace of the opening process.

However, a process writing a mapping must be either in the child user namespace or in the parent user namespace (and the right capabilities).

As an example of defining a user ID mapping, first we note the PID of the bash shell in the user namespace:

```console
nobody@ubu:~$ ps af -o 'pid uid command'
    PID   UID COMMAND
   3824 65534 bash
   4445 65534  \_ -bash
   4455 65534      \_ ps af -o pid uid command
...
```

We define a mapping by writing to `/proc/4445/uid_map` from the bash shell in the parent user namespace:

```console
ca@ubu:~$ ps af -o 'pid uid command'
    PID   UID COMMAND
   3824  1000 bash
   4445  1000  \_ -bash
...
ca@ubu:~$ echo '0 1000 1' > /proc/4445/uid_map
```

The user ID of the bash shell in the child user namespace is now 0:

```console
nobody@ubu:~$ ps af -o 'pid uid command'
    PID   UID COMMAND
   3824     0 bash
   4445     0  \_ -bash
   4489     0      \_ ps af -o pid uid command
...
```

We can only perform this mapping once per user namespace:

```console
ca@ubu:~$ echo '0 1000 1' > /proc/4445/uid_map
bash: echo: write error: Operation not permitted
ca@ubu:~$ sudo echo '0 1000 1' > /proc/4445/uid_map
[sudo] password for ca: 
echo: write error: Operation not permitted
```

### Viewing user ID mappings
Here is an example of different user namespaces viewing a user ID mapping for a process.
First, we create two children user namespaces:

```console
# shell 1
ca@ubu:~$ unshare -U
nobody@ubu:~$ echo $$
4970
```

```console
# shell 2
ca@ubu:~$ unshare -U
nobody@ubu:~$ echo $$
5035
```

Then we define user ID mappings from the initial user namespace:

```console
# shell 3
ca@ubu:~$ echo '0 1000 1' > /proc/4970/uid_map
ca@ubu:~$ cat /proc/4970/uid_map
         0       1000          1
ca@ubu:~$ echo '200 1000 1' > /proc/5035/uid_map
ca@ubu:~$ cat /proc/5035/uid_map
       200       1000          1
```

From the first child user namespace, we view the user ID mapping of the process in the second child user namespace.
`ID-outside-ns` is shown with respect to the user ID in the first child user namespace that maps to the user ID of the parent user namespace defined in the user ID mapping that we created for the process in the second child user namespace:
```console
# shell 1
nobody@ubu:~$ cat /proc/5035/uid_map
       200          0          1
```

Similarly when viewing the user ID mapping of the process in the first user namespace from the second user namespace:

```console
# shell 2
nobody@ubu:~$ cat /proc/4970/uid_map
         0        200          1
```