# Questions

- When would you use `pivot_root` without a mount namespace?
- Why not use Docker or bubblewrap for sandboxing?