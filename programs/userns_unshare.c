/* demo_userns.c

   Copyright 2013, Michael Kerrisk
   Licensed under GNU General Public License v2 or later

   Demonstrate the use of the clone() CLONE_NEWUSER flag.

   Link with "-lcap" and make sure that the "libcap-devel" (or
   similar) package is installed on the system.
*/
#define _GNU_SOURCE
#include <sys/capability.h>
#include <sys/wait.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \
                        } while (0)

int
main(int argc, char *argv[])
{
    int res;
    cap_t caps;

    res = unshare(CLONE_NEWUSER);
    if (res == -1)
        errExit("unshare");

    caps = cap_get_proc();
    printf("capabilities: %s\n", cap_to_text(caps, NULL));

    exit(EXIT_SUCCESS);
}